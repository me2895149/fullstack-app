import express, { Response, Request } from 'express'
import 'dotenv/config'
import { findLanguages } from './db'

const server = express()
server.use('/', express.static('./dist/client'))

const languages = ['Javascript','Python','Go','Java','Kotlin','Scala','C','Swift','TypeScript','Ruby']

server.get('/languages', async (req: Request, res: Response) => {
    res.send(await findLanguages())   
})


const { PORT } = process.env
server.listen(PORT, () => {
	console.log('Products API listening to port', PORT)
})
