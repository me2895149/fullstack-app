import { useEffect, useState } from 'react'

function App() {
  const [languages, setLanguages] = useState<Array<Language>>([])

  useEffect(() => {
    initialize()
  }, [])

  interface Language {
    id: number
    language: string
  }

  const initialize = async () => {
    const result = await fetch('/languages')
    const languages = await result.json()
    setLanguages(languages)
  }

  return (
    <>
      <ul>
        {languages.map((language)  => {
          return (
            <li>{language.language}</li>
          )
        })}
      </ul>
    </>
  )
}

export default App
